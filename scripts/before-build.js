#!/usr/bin/env node

var fs = require('fs');
const currentdir = process.cwd();
const originfile = currentdir + '/build-extras.gradle';
const destfile = currentdir + '/platforms/android/app/build-extras.gradle';

console.log(`Copying ${originfile} to ${destfile}`)

fs.copyFile(originfile, destfile, err => {
    if (err) throw err;
    console.log(`${originfile} was copied`);
});
