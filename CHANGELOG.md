# v1.1

- Fix: copy as RGB in color picker
- Replace card actions with menu in color preview
- Add color code in color preview

# v1.0.0

- Palette
- Picker
- About page
