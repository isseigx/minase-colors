var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handle
    onDeviceReady: function() {
        document.addEventListener("menubutton", onMenuKeyDown, false);
    },
    
    onMenuKeyDown: function() {
        let menu = M.Dropdown.getInstance(document.getElementById('menu-dt'));
        
        if (menu)
            menu.open();
    }
};

app.initialize();

// DOM init
var appvm = undefined;

document.addEventListener('DOMContentLoaded', () => {
    appvm = new AppViewModel();
    ko.applyBindings(appvm);
    
    //Materialize init
    M.Modal.init(document.querySelectorAll('.modal'), {inDuration: 350, outDuration: 350});
    M.Dropdown.init(document.getElementById('menu-dt'), {constrainWidth: false});
});

function AppViewModel() {
    
    this.colors = ko.observableArray(MaterialColors.Red);
    this.title = ko.observable('Red');
    
    this.setColors = colorName => {
        this.colors(getColors(colorName));
        this.title(colorName);
    }
    
    this.copy = color => {
        try {
            cordova.plugins.clipboard.copy(color);
            M.toast({html: 'Color copied to clipboard'});
        } catch(error) {
            console.error(error);
        }
    }
    
    this.page = ko.observable(0);
    this.page.subscribe(updatePages);
    this.setPage = page => this.page(page);
    
    this.red = ko.observable(63).extend({rateLimit: 100});
    this.green = ko.observable(81).extend({rateLimit: 100});
    this.blue = ko.observable(181).extend({rateLimit: 100});
    
    this.generatedColor = ko.computed(() => {
        return RGBToHex(Number(this.red()), Number(this.green()), Number(this.blue()));
    }, this);
    
    this.generatedColorRGB = ko.computed(() => {
        return 'rgb(' + this.red() + ', ' + this.green() + ', ' + this.blue() + ')'
    }, this);
    
    this.copyRGB = () => {
        try {
            cordova.plugins.clipboard.copy(this.generatedColorRGB());
            M.toast({html: 'Copied as RGB'});
        } catch (error) {
            console.error(error);
        }
    }
    
    this.copyHex = () => {
        try {
            cordova.plugins.clipboard.copy(this.generatedColor());
            M.toast({html: 'Copied as Hexadecimal'});
        } catch (error) {
            console.error(error);
        }
    }

    this.generatedTextColor = ko.computed(() => {
        const value = (Number(this.red()) + Number(this.green()) + Number(this.blue())) / 3;
        if (value > 127)
            return 'black';

        return 'white';
    });
}

function getColors(colorName) {
    switch (colorName) {
        
        case 'Red':
            return MaterialColors.Red;
        case 'Pink':
            return MaterialColors.Pink;
        case 'Purple':
            return MaterialColors.Purple;
        case 'Deep Purple':
            return MaterialColors.Deep_Purple;
        case 'Indigo':
            return MaterialColors.Indigo;
        case 'Blue':
            return MaterialColors.Blue;
        case 'Light Blue':
            return MaterialColors.Light_Blue;
        case 'Cyan':
            return MaterialColors.Cyan;
        case 'Teal':
            return MaterialColors.Teal;
        case 'Green':
            return MaterialColors.Green;
        case 'Light Green':
            return MaterialColors.Light_Green;
        case 'Lime':
            return MaterialColors.Lime;
        case 'Yellow':
            return MaterialColors.Yellow;
        case 'Amber':
            return MaterialColors.Amber;
        case 'Orange':
            return MaterialColors.Orange;
        case 'Deep Orange':
            return MaterialColors.Deep_Orange;
        case 'Brown':
            return MaterialColors.Brown;
        case 'Gray':
            return MaterialColors.Gray;
        case 'Blue Gray':
            return MaterialColors.Blue_Gray;
        default:
            return [];
        
    }
}

// https://stackoverflow.com/a/39077686

const RGBToHex = (r, g, b) => '#' + [r, g, b].map(x => {
    const hex = x.toString(16);
    return hex.length === 1 ? '0' + hex : hex;
}).join('');

const hexToRGB = hex =>
  hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
             ,(m, r, g, b) => '#' + r + r + g + g + b + b)
    .substring(1).match(/.{2}/g)
    .map(x => parseInt(x, 16))

function updatePages(pageid) {
    switch (pageid) {
        case 0:
            appvm.setColors('Red');
            break;
        case 1:
            appvm.title('Color Picker');
            setupRanges();
            break;
        default:
            appvm.title('About');
    }
}

let setupRanges = () => {
    setTimeout(initRanges, 500);
}

let initRanges = () => {
    const ranges = document.querySelectorAll('input[type=range]');
    M.Range.init(ranges);
    
    //init preview menu
    const ddoption = {
        constrainWidth: false,
        alignment: 'right',
        container: document.querySelector('body')
    }
    
    M.Dropdown.init(document.getElementById('preview-menu-dt'), ddoption);
}
