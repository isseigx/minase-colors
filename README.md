![](res/icon/android/mipmap-xhdpi/minase_ic_launcher.png)

## Minase

Minase is a palette application for android based on Apache Cordova framework (I wanted to explore the framework and develop an android application without the monster called Android Studio)

The UI is develop with [Materialize](https://materializecss.com) for Material look and [knockout.js](https://knockoutjs.com) for UI async refresh.

The application lets you choose between all colors of Material Design spec and generate your own colors with the color picker.

### Building from source code

Required tools:

- Android SDK
- Node.js
- Apache Cordova

Follow [these steps](https://cordova.apache.org/#getstarted) to install Cordova

And execute these commands

```
git clone https://gitlab.com/isseigx/minase-colors.git
cd minase-colors
cordova build

```